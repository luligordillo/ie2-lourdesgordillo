from faulthandler import disable
from logging import root
import tkinter as tk
from tkinter import StringVar, ttk
from turtle import width
from ttkbootstrap import Style
from centrar_ventana import centrar

def validar_entrada(accion, texto_previo, texto_nuevo, texto):
    print('1 Acción', accion)
    print('2 Texto previo', texto_previo)
    print('3 Texto que se está ingresando', texto_nuevo)
    print('4 Texto si el resultado es True', texto)
    
    if texto_nuevo.isdigit():

     return True
    else:
     return False 




#Tamaño del formulario, ancho=360, alto=249, puede usar centrar_ventana.
Style = Style('darkly')
ventana = tk.Tk()

ventana.geometry(centrar(ancho=360, alto=249, app= ventana))

#Incluir el título: IE2 - Programación II
ventana.title(' IE2 - PROGRAMACION II')



entry1= tk.Entry(ventana, validate='key', width=15)
entry2= tk.Entry(ventana, validate='key', width=15)
entry3= tk.Entry(ventana, validate='key', width=15,state='disabled')
ventana.mainloop


#En la línea siguiente al crear el formulario o la ventana, aplicar el estilo con:
#Style = Style(‘darkly’)

#Insertar un frame (marco) con el nombre frame
frame = ttk.Frame(root, padding='10 10 10 10', height=70, width=30, borderwidth=2, relief='sunken')
frame.pack()
     #a- Cada widget debe estar en una grilla dentro del frame con un padx=5 y un pady=5.
frame.grid(row=0, column=0, padx=5, pady=5)

#Insertar tres label con el nombre label1, label2, y label3 como muestra la imágen, con su respectivo text y width=15.
label1 = tk.Label(ventana, text= 'valor 1: ', width=15)

label2 = tk.Label(ventana, text= 'valor 2: ', width=15)

label3 = tk.Label(ventana, text= 'resultado: ', width=15)

label1.grid(ventana,column=0,row=0, padx= 5, pady= 5)
label2.grid(ventana,column=1, row=1, padx=5, pady=5)
label3.grid(ventana,column=2,row=2, padx= 5, pady=5)



#Insertar seis botones en tres filas, dos en cada una con las consignas de la definición de sus respectivas funciones serán entregadas y realizadas en el instituto en el horario y fecha de la IE2.
boton1 = tk.Button(ventana,text= '+', bg= 'azul').grid(row=0,column=4)
boton2 = tk.Button(ventana,text='*', bg='azul').grid(row=0, column=5)
boton3 = tk.Button(ventana,text = '%', bg='azul').grid(row=0, column=6)
boton4 = tk.Button(ventana,text='-', bg= 'azul').grid(row=1, column=4)
boton5 = tk.Button(ventana, text='/', bg='azul').grid(row=1, column=5)
boton6 = tk.Button(ventana,text='LIMPIAR', bg='azul').grid(row=1,column=6)
#Definir una función limpiar que responda al botón con el mismo nombre referenciando la función con la propiedad command=limpiar desde el entry3:
def limpiar():
    entry1.delete(0, tk.END)

    entry2.delete(0, tk.END)

    entry3.config(state='normal')
    entry3.delete(0, tk.END)
    entry3.config(state='disabled')

    entry1.focus()

    return





